package com.plexer0.nitter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.app.Activity;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class LinkActivity extends Activity {

    // Initialise WebView
    private WebView wv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link);
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        wv = (WebView) findViewById(R.id.webView);
        wv.setWebViewClient(new NitterClient()
        {
            // Rewrite standard Twitter URLs to Nitter
            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }
        });

        wv.getSettings().setLoadsImagesAutomatically(true);
        wv.getSettings().setJavaScriptEnabled(sharedPrefs.getBoolean("javascript_toggle", true));
        wv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wv.setBackgroundColor(0xF4444444);
        if (savedInstanceState == null)
        {
            String load_url = String.valueOf(getIntent().getData());
            String base_domain = Uri.parse(String.valueOf(getIntent().getData())).getHost();
            if(base_domain.equals("twitter.com") || base_domain.equals("mobile.twitter.com"))
            {
                load_url = load_url.replace("mobile.twitter.com", "nitter.net");
                load_url = load_url.replace("twitter.com", "nitter.net");
                // Toast.makeText(LinkActivity.this, "URL changed: "+load_url, Toast.LENGTH_LONG).show();
            }
            // Toast.makeText(LinkActivity.this, "URL static: "+load_url, Toast.LENGTH_LONG).show();
            wv.loadUrl(load_url); // Leave as is to fetch URL intent.
        }
    }

    // Prevent WebView from being destroyed when the device orientation changes
    @Override
    protected void onSaveInstanceState(Bundle outState )
    {
        super.onSaveInstanceState(outState);
        wv.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        wv.restoreState(savedInstanceState);
    }

    // Go backwards in WebView rather than exiting the app
    @Override
    public void onBackPressed() {
        if(wv!= null && wv.canGoBack())
            wv.goBack();
        else
            super.onBackPressed();
    }

    private class NitterClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            wv.canGoBack();
            view.loadUrl(url);
            return true;
        }
    }
}
